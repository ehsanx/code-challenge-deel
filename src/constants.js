const ContractStatus = {
  NEW: 'new',
  IN_PROGRESS: 'in_progress',
  TERMINATED: 'terminated',
};

// The maximum percentage of a user's total of jobs to pay that he can deposit at one.
// Used in POST /balances/deposit/:userId API.
const MAX_DEPOSIT_TRESHHOLD = 0.25;

// The default number of records /admin/best-clients API should return if no limit param is provided
const BEST_CLIENTS_DEFAULT_LIMIT = 2;

module.exports = { ContractStatus, MAX_DEPOSIT_TRESHHOLD, BEST_CLIENTS_DEFAULT_LIMIT };
