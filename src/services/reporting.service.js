const { Op } = require('sequelize');
const {
  sequelize,
  Job,
  Contract,
  Profile,
} = require('../model');
const { dateUtils, numberUtils } = require('../utils');

const getBestProfession = async (startDate, endDate) => {
  if (!dateUtils.isValidDate(startDate) || !dateUtils.isValidDate(endDate)) {
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'The date range parameters are not valid',
    };
  }

  // The join order can affect performance, but this varies depending on the specific data and
  // database system. I started with the Profile table since it has the smallest amount of data.
  // It can potentially reduce the number of rows joined in subsequent steps. Nevertheless, it
  // may be beneficial to start with Jobs table, since it contains most of the query conditions.
  // The optimal approach should be determined against the specific use case.
  const bestProfession = await Profile.findOne({
    attributes: ['profession', [sequelize.fn('SUM', sequelize.col('price')), 'totalEarned']],
    include: [
      {
        model: Contract,
        as: 'Contractor',
        attributes: [],
        required: true,
        include: [
          {
            model: Job,
            required: true,
            attributes: [],
            where: {
              paid: true,
              paymentDate: {
                [Op.between]: [startDate, endDate],
              },
            },
          },
        ],
      },
    ],
    where: {
      type: 'contractor',
    },
    group: ['profession'],
    order: [[sequelize.col('totalEarned'), 'DESC']],
    limit: 1,
    subQuery: false,
  });

  if (bestProfession === null) {
    // Returning an error response in case of no completed job in the requested date range.
    // It might be needed to change.
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'No information is available for the requested date range',
    };
  }

  return { // TODO: We'd better create a separate class for this
    success: true,
    data: bestProfession,
  };
};

const getBestClients = async (startDate, endDate, limit) => {
  if (!dateUtils.isValidDate(startDate) || !dateUtils.isValidDate(endDate)) {
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'The date range parameters are not valid',
    };
  }

  if (!numberUtils.isPositiveInteger(limit)) {
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'The limit parameters are not valid',
    };
  }

  const bestClients = await Job.findAll({
    attributes: [
      [sequelize.fn('SUM', sequelize.col('price')), 'paid'],
    ],
    include: {
      model: Contract,
      attributes: ['id'],
      include: {
        model: Profile,
        as: 'Client',
        attributes: [
          'id',
          'firstName',
          'lastName',
        ],
      },
    },
    where: {
      paid: true,
      paymentDate: {
        [Op.between]: [startDate, endDate],
      },
    },
    group: ['Contract.Client.id'],
    order: [['paid', 'DESC']],
    limit,
  });

  if (bestClients === null) {
    // Returning an error response in case of no completed job in the requested date range.
    // It might be needed to change.
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'No information is available for the requested date range',
    };
  }

  // Format the output as defined
  const bestClientsFormatted = bestClients.map((client) => {
    const { Contract: { Client: { id, firstName, lastName } }, paid } = client;
    const fullName = `${firstName} ${lastName}`;
    return { id, fullName, paid };
  });

  return { // TODO: We'd better create a separate class for this
    success: true,
    data: bestClientsFormatted,
  };
};

module.exports = {
  getBestProfession,
  getBestClients,
};
