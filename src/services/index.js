module.exports.contractsService = require('./contracts.service');
module.exports.jobsService = require('./jobs.service');
module.exports.paymentService = require('./payment.service');
module.exports.reportingService = require('./reporting.service');
