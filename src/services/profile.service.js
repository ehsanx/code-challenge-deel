const { Profile } = require('../model');

const getProfileById = async (id, queryOptions) => Profile.findByPk(id, queryOptions);

const increaseBalance = async (profile, amount, queryOptions) => profile.increment(
  { balance: amount },
  queryOptions,
);

const decreaseBalance = async (profile, amount, queryOptions) => profile.decrement(
  { balance: amount },
  queryOptions,
);

module.exports = {
  getProfileById,
  increaseBalance,
  decreaseBalance,
};
