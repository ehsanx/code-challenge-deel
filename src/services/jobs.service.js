const { Op } = require('sequelize');
const { sequelize, Contract, Job } = require('../model');
const { ContractStatus } = require('../constants');

const getOngoingUnpaidJobsByUserId = (userId) => Job.findAll({
  include: {
    model: Contract,
    attributes: [],
    required: true,
    where: {
      status: ContractStatus.IN_PROGRESS,
      [Op.or]: [
        { ContractorId: userId },
        { ClientId: userId },
      ],
    },
  },
  where: {
    paid: {
      [Op.or]: [false, null],
    },
  },
});

const findUnpaidJobByJobIdAndClientId = async (jobId, userId, queryOptions) => {
  try {
    const job = await Job.findOne(
      {
        where: {
          id: jobId,
          paid: { [Op.or]: [false, null] },
        },
        include: [
          {
            model: Contract,
            required: true,
            attributes: ['ContractorId'],
            where: {
              ClientId: userId,
              status: { [Op.ne]: ContractStatus.TERMINATED },
            },
          },
        ],
      },
      queryOptions,
    );
    return job;
  } catch (error) {
    console.log(error); // TODO: We'd better employ a better error handling and logging system
    return null;
  }
};

const markJobAsPaid = async (job, queryOptions) => job.update(
  {
    paid: true,
    paymentDate: new Date(),
  },
  queryOptions,
);

const getTotalUnpaidJobPriceForClient = async (clientId, queryOptions) => {
  const unpaidJobsAggregated = await Job.findOne(
    {
      attributes: {
        include: [[sequelize.fn('SUM', sequelize.col('price')), 'priceSum']],
      },
      include: [
        {
          model: Contract,
          required: true,
          attributes: [],
          where: {
            ClientId: clientId,
            status: ContractStatus.IN_PROGRESS,
          },
        },
      ],
      where: {
        // The paid attribute of default job records is null, so we check either false or null
        paid: { [Op.or]: [false, null] },
      },
    },
    queryOptions,
  );

  const { priceSum } = unpaidJobsAggregated.dataValues;

  return priceSum;
};

module.exports = {
  getOngoingUnpaidJobsByUserId,
  findUnpaidJobByJobIdAndClientId,
  markJobAsPaid,
  getTotalUnpaidJobPriceForClient,
};
