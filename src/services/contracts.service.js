const { Op } = require('sequelize');
const { Contract } = require('../model');
const { ContractStatus } = require('../constants');

const getContract = (id) => Contract.findOne({ where: { id } });

const isContractBelongsToUser = (contract, userId) =>
  contract.ContractorId === userId || contract.ClientId === userId;

const getActiveContractsByUserId = (userId) => Contract.findAll({
  where: {
    status: {
      [Op.ne]: ContractStatus.TERMINATED,
    },
    [Op.or]: [
      { ContractorId: userId },
      { ClientId: userId },
    ],
  },
});

module.exports = {
  getContract,
  isContractBelongsToUser,
  getActiveContractsByUserId,
};
