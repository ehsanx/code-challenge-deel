const { sequelize } = require('../model');
const jobsService = require('./jobs.service');
const profileService = require('./profile.service');
const { MAX_DEPOSIT_TRESHHOLD } = require('../constants');

const payForAJob = async (clientId, jobId) => {
  // Developer note: The required information can also be retrieved using a single SQL query with a
  // few joins. I've decided to split this query into smaller parts to improve code maintainability
  // and readability, even though it may affect performance.
  // Each subquery retrieves a specific set of data and can be tested and optimized separately.
  // This should make the code easier to work with in the long run. However, we should continue to
  // monitor performance and consider improvements if needed.

  // TODO: We'd better split this function into multiple ones to make it shorter

  let transaction;
  try {
    transaction = await sequelize.transaction({isolationLevel: 'SERIALIZABLE', type: 'IMMEDIATE' });
  } catch (error) {
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: "We can't perform this right now. please try again later.",
    };
  }
  try {
    // Query the Job model to find a job with the given jobId
    // The paid attribute of default job records is null, so we check paid is either false or null
    const job = await jobsService.findUnpaidJobByJobIdAndClientId(jobId, clientId, { transaction });
    if (!job) {
      throw new Error(`There is no unpaid job, id=${jobId}.`);
    }
    const { price, Contract: { ContractorId: contractorId } } = job;

    const client = await profileService.getProfileById(clientId, { transaction });
    if (!client) { // This should not actually happen, but every module should handle its exceptions
      throw new Error(`There is no unpaid job, id=${jobId}.`);
    }
    const clientBalance = client.balance;

    // Check if the client have enough balance
    if (clientBalance < price) {
      throw new Error('Insufficient balance.');
    }

    // Decrement the client's balance
    await profileService.decreaseBalance(client, price, { transaction });

    // Increment the contractor's balance
    const contractor = await profileService.getProfileById(contractorId, { transaction });
    await profileService.increaseBalance(contractor, price, { transaction });

    // Update the job's paid status and payment date
    await jobsService.markJobAsPaid(job, { transaction });

    await transaction.commit();
  } catch (error) {
    // If any of the steps has failed, rollback the transaction
    await transaction.rollback();
    console.log(error); // TODO: We'd better employ a better logging system
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: error.message,
    };
  }

  return { // TODO: We'd better create a separate class for this
    success: true,
    message: `Payment for job ${jobId} is done`,
  };
};

const makeDeposit = async (clientId, amount) => {
  if (typeof amount !== 'number' || amount <= 0) { // Negative values or other datatypes are not welcome
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: 'The provided amount is invalid',
    };
  }

  let transaction;
  try {
    transaction = await sequelize.transaction({isolationLevel: 'SERIALIZABLE', type: 'IMMEDIATE' });
  } catch (error) {
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: "We can't perform this right now. please try again later.",
    };
  }
  try {
    const client = await profileService.getProfileById(clientId, { transaction });

    const unpaidJobsAmount = await jobsService.getTotalUnpaidJobPriceForClient(
      clientId,
      { transaction },
    );
    if (unpaidJobsAmount === null) {
      throw new Error('The client has no unpaid jobs');
    }

    const depositThreshold = unpaidJobsAmount * MAX_DEPOSIT_TRESHHOLD;
    if (amount > depositThreshold) {
      throw new Error(`The client can deposit no more than ${depositThreshold}`);
    }

    await profileService.increaseBalance(client, amount, { transaction });

    await transaction.commit();
  } catch (error) {
    await transaction.rollback();
    console.log(error); // TODO: We'd better employ a better logging system
    return { // TODO: We'd better create a separate class for this
      success: false,
      message: error.message,
    };
  }

  return { // TODO: We'd better create a separate class for this
    success: true,
    message: `A credit of ${amount} has been issued to client ${clientId}`,
  };
};

module.exports = {
  payForAJob,
  makeDeposit,
};
