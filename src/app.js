const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { sequelize } = require('./model');
const routes = require('./routes');

const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize);
app.set('models', sequelize.models);

// TODO: This should be configured precisely
app.use(cors());

app.use('/', routes);

module.exports = app;
