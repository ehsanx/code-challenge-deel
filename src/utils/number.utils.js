// eslint-disable-next-line eqeqeq
const isPositiveInteger = (num) => parseInt(num, 10) == num && num > 0;

module.exports = {
  isPositiveInteger,
};
