const { reportingService } = require('../services');
const { BEST_CLIENTS_DEFAULT_LIMIT } = require('../constants');

const getBestProfession = async (req, res) => {
  // TODO: Authorize admin, it can be done in getProfile middleware
  const { start, end } = req.query;

  // TODO: Date format should be defined and an input validation routine should be placed in action
  const startDate = new Date(start);
  const endDate = new Date(end);

  const { success, message, data: bestProfession } =
    await reportingService.getBestProfession(startDate, endDate);
  if (!success) {
    return res.status(500).json({ message }).end();
  }

  return res.json({ bestProfession });
};

const getBestClients = async (req, res) => {
  // TODO: Authorize admin, it can be done in getProfile middleware
  const { start, end, limit = BEST_CLIENTS_DEFAULT_LIMIT } = req.query;

  // TODO: input validation routine should be placed in action
  const startDate = new Date(start);
  const endDate = new Date(end);

  const { success, message, data: bestClients } =
    await reportingService.getBestClients(startDate, endDate, limit);
  if (!success) {
    return res.status(500).json({ message }).end();
  }

  return res.json(bestClients);
};

module.exports = {
  getBestProfession,
  getBestClients,
};
