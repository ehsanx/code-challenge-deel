module.exports.contractsController = require('./contracts.controller');
module.exports.jobsController = require('./jobs.controller');
module.exports.paymentController = require('./payment.controller');
module.exports.adminController = require('./admin.controller');
