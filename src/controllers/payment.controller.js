const { paymentService } = require('../services');

const payForAJob = async (req, res) => {
  const userId = req.profile.id;
  const { job_id: jobId } = req.params;

  const { success, message } = await paymentService.payForAJob(userId, jobId);
  if (!success) {
    return res.status(500).json({ message }).end();
  }

  return res.json({ message });
};

const deposit = async (req, res) => {
  const { userId } = req.params;
  const { amount } = req.body;

  const { success, message } = await paymentService.makeDeposit(userId, amount);
  if (!success) {
    return res.status(500).json({ message }).end();
    // TODO: We'd better not to expose every internal detail in the production environment
  }

  return res.json({ message });
};

module.exports = {
  payForAJob,
  deposit,
};
