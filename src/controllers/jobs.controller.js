const { jobsService } = require('../services');

const getUnpaid = async (req, res) => {
  const userId = req.profile.id;
  const jobs = await jobsService.getOngoingUnpaidJobsByUserId(userId);
  return res.json(jobs);
};

module.exports = {
  getUnpaid,
};
