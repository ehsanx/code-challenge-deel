const { contractsService } = require('../services');

const getContract = async (req, res, next) => {
  const { id } = req.params;

  const contract = await contractsService.getContract(id);
  if (!contract) {
    return res.status(404).end();
  }

  const userId = req.profile.id;
  const isContractBelongsToUser = contractsService.isContractBelongsToUser(contract, userId);
  if (!isContractBelongsToUser) {
    return res.status(403).end();
  }

  return res.json(contract);
};

const getContracts = async (req, res) => {
  const userId = req.profile.id;
  const contracts = await contractsService.getActiveContractsByUserId(userId);
  return res.json(contracts);
};

module.exports = {
  getContract,
  getContracts,
};
