const express = require('express');
const { contractsController } = require('../controllers');
const { getProfile } = require('../middleware/getProfile');

const router = express.Router();

router
  .route('/:id')
  .get(getProfile, contractsController.getContract);

router
  .route('/')
  .get(getProfile, contractsController.getContracts);

module.exports = router;
