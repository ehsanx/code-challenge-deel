const express = require('express');
const { adminController } = require('../controllers');
const { getProfile } = require('../middleware/getProfile');

const router = express.Router();

router
  .route('/best-profession')
  .get(getProfile, adminController.getBestProfession);

router
  .route('/best-clients')
  .get(getProfile, adminController.getBestClients);

module.exports = router;
