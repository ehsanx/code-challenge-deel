const express = require('express');
const contractsRoute = require('./contracts.routes');
const jobsRoute = require('./jobs.routes');
const balancesRoute = require('./balances.routes');
const adminRoute = require('./admin.routes');

const router = express.Router();

const routes = [
  {
    path: '/contracts',
    route: contractsRoute,
  },
  {
    path: '/jobs',
    route: jobsRoute,
  },
  {
    path: '/balances',
    route: balancesRoute,
  },
  {
    path: '/admin',
    route: adminRoute,
  },
];

routes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;
