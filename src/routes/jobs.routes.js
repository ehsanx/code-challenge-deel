const express = require('express');
const { jobsController } = require('../controllers');
const { paymentController } = require('../controllers');
const { getProfile } = require('../middleware/getProfile');

const router = express.Router();

router
  .route('/unpaid')
  .get(getProfile, jobsController.getUnpaid);

router
  .route('/:job_id/pay') // TODO: We'd better follow the naming convention
  .post(getProfile, paymentController.payForAJob);

module.exports = router;
