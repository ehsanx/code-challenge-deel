const express = require('express');
const { paymentController } = require('../controllers');

const router = express.Router();

router
  .route('/deposit/:userId')
  .post(paymentController.deposit);

module.exports = router;
