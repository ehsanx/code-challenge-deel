import Contracts from './pages/Contracts';

const App = () => (
  <div className="md:container md:mx-auto py-5">
    <Contracts />
  </div>
);

export default App;
