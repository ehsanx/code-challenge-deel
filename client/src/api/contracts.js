import { useEffect, useState } from 'react';
import { api } from './axios';

// TODO
const getContracts = (profileId) => api.get('/contracts', { headers: { profile_id: profileId } });

const useContractsQuery = (profileId) => {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getContracts(profileId).then((contracts) => {
      setData(contracts);
      setLoading(false);
    });
  }, []);

  return { data, loading };
};

export default useContractsQuery;
