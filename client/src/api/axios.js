import axios from 'axios';
import config from '../config';

const getHeaders = () => (
  {
    'Content-Type': 'application/json',
  }
);

export const API_DEFAULTS = {
  baseURL: config.baseURL,
  headers: getHeaders(),
};

const api = axios.create({
  baseURL: config.baseUrl,
});

api.interceptors.response.use(
  (response) => {
    try {
      const { data } = response;
      return data;
    } catch (error) {
      // TODO: handle api errors
      return error;
    }
  },
  (error) => {
    if (
      error.response.status === 401
    ) {
      window.location.href = '/';
    }
  },
);

const setRequestInterceptors = () => {
  api.interceptors.request.use((conf) => {
    const defaultHeaders = getHeaders();
    const modifiedConf = {
      ...conf,
      headers: { ...defaultHeaders, ...conf.headers },
    };
    return modifiedConf;
  });
};

setRequestInterceptors();

export { api, setRequestInterceptors };
