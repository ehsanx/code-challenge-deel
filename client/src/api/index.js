import { api } from './axios';
import useContractsQuery from './contracts';

export { useContractsQuery, api };
