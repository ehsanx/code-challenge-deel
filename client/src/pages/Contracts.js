import { dateHelper } from '../helpers';

const { useContractsQuery } = require('../api');

const Contracts = () => {
  const { data, loading } = useContractsQuery(6); // TODO: Parametrize profile id

  if (loading) {
    return 'Loading...';
  }

  if (!data.length) {
    return 'No data found';
  }

  return (
    <table className="min-w-full text-left text-sm font-light">
      <thead className="border-b font-medium">
        <tr>
          <th scope="col" className="px-6 py-4">ID</th>
          <th scope="col" className="px-6 py-4">Terms</th>
          <th scope="col" className="px-6 py-4">Status</th>
          <th scope="col" className="px-6 py-4">Created At</th>
          <th scope="col" className="px-6 py-4">Updated At</th>
        </tr>
      </thead>
      <tbody>
        {data.map((row) => (
          <tr key={row.id} className="border-b">
            <td className="whitespace-nowrap px-6 py-4">{row.id}</td>
            <td className="whitespace-nowrap px-6 py-4">{row.terms}</td>
            <td className="whitespace-nowrap px-6 py-4">{row.status}</td>
            <td className="whitespace-nowrap px-6 py-4">{dateHelper.format(row.createdAt)}</td>
            <td className="whitespace-nowrap px-6 py-4">{dateHelper.format(row.updatedAt)}</td>
          </tr>
        ))}

      </tbody>
    </table>
  );
};

export default Contracts;
